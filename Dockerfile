FROM ubuntu:latest

RUN apt-get update && \
  apt-get install -y \
  ca-certificates \
  curl \
  git \
  build-essential \
  libssl-dev \
  zlib1g-dev

RUN curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash

ENV PATH="/root/.pyenv/bin:$PATH"
RUN pyenv install 3.6.3 && \
  pyenv global 3.6.3

RUN eval "$(pyenv init -)" && pip install \
  requests

COPY python.sh python.sh
RUN chmod ugo+x python.sh
ENTRYPOINT ["/python.sh"]
CMD ["python","--version"]
